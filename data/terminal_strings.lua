-- terminal_strings.lua

terminal = {}
terminal.textItems = {
    {
        -- String, xy coords, size(optional), color(optional)
        {"Demoparty", {0,0}, 8, {0,1,0,}},
        {"within range", {.0,.14*3}, 8, {0,1,0,}},
    },
    {
        {"Scanning", {0,0}, 8, {0,1,0,}},
        {"...", {.0,.14*1}, 8, {0,1,0}},
        {"...", {.0,.14*2}, 8, {0,1,0}},
        {"...", {.0,.14*3}, 8, {0,1,0}},
        {"BEER DETECTED", {.0,.14*4}, 8, {1,0,0}},
    },
    {
        {"Planet", {0,0}, 8, {0,1,0,}},
        {"surface", {.0,.14}, 8, {0,1,0}},
        {"appears", {.0,.14*2}, 8, {0,1,0}},
        {"pristine.", {.0,.14*3}, 8, {0,1,0}},
    },
    {
        {"COMMENCE", {.17,.14}, 8, {0,1,0,}},
        {"DEMO", {.3,.14*3}, 8, {1,0,0}},
        {"BOMBARDMENT", {.06,.14*4}, 8, {1,0,0}},
    },
    {
        {"Surface", {.15,.14}, 8, {0,1,0,}},
        {"damage", {.15,.14*2}, 8, {0,1,0}},
        {".....", {.15,.14*3}, 8, {0,1,0}},
        {"minimal.", {.15,.14*4}, 8, {1,0,0}},
    },
    {
        {"Need more", {0,0}, 8, {1,1,0,}},
        {"demo ammo!!", {.0,.14}, 8, {1,1,0}},
        {"Calling all", {.0,.14*3}, 8, {1,.5,0}},
        {"Demomakers!!", {.0,.14*4}, 8, {1,.5,0}},
    },
    {
        {"FIRE", {0.15,0.4}, 16, {1,1,0,}},
    },
}

-- Just throw in some inline logic...
local psap = terminal.textItems[3]
for i=1,#psap do
    psap[i][2][1] = psap[i][2][1] + .16
    psap[i][2][2] = psap[i][2][2] + .2
end
