--[[ text_over_scene.lua

    Draw text over a scene
]]
text_over_scene = {}

text_over_scene.__index = text_over_scene

function text_over_scene.new(...)
    local self = setmetatable({}, text_over_scene)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

require("util.glfont")
local mm = require("util.matrixmath")

function text_over_scene:init()
    self.vbos = {}
    self.vao = 0
    self.prog = 0
    self.dataDir = nil
    self.glfont = nil

    self.InnerScene = require("scene.starfield").new()
    self.showtext = true
end
function text_over_scene:setDataDirectory(dir)
    self.InnerScene:setDataDirectory(dir)
    self.dataDir = dir
end

function text_over_scene:initGL()
    self.InnerScene:initGL()

    dir = "fonts"
    if self.dataDir then dir = self.dataDir .. "/" .. dir end
    self.glfont = GLFont.new('orbitron_512.fnt', 'orbitron_512_0.raw')
    self.glfont:setDataDirectory(dir)
    self.glfont:initGL()
end

function text_over_scene:exitGL()
    self.InnerScene:exitGL()
    self.glfont:exitGL()
end

function text_over_scene:renderOverlay()
    local sz = sz or .6
    local col = col or {1, 1, 1}
    local xy = {.05,.85}
    local text = "A remote corner of demo space"

    local w,h = 1920/2, 1080/2
    local p = {}
    mm.glh_ortho(p, 0, w, h, 0, -1, 1)
    gl.glDisable(GL.GL_DEPTH_TEST)
    do
        local m = {}
        mm.make_identity_matrix(m)

        mm.glh_translate(m, xy[1]*w, xy[2]*h, 0)
        mm.glh_scale(m, sz, sz, sz)

        self.glfont:render_string(m, p, col, text)
    end
    gl.glEnable(GL.GL_DEPTH_TEST)
end

function text_over_scene:renderEye(model, view, proj)
    self.InnerScene:renderEye(model, view, proj)
    if self.showtext then
        self:renderOverlay()
    end
end

function text_over_scene:timestep(absTime, dt)
    if self.InnerScene.timestep then
        self.InnerScene:timestep(absTime, dt)
    end
end

function text_over_scene:keypressed(key, scancode, action, mods)
    if self.InnerScene.keypressed then
        self.InnerScene:keypressed(key, scancode, action, mods)
    end
end

return text_over_scene
