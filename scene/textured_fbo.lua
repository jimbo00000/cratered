--[[ textured_fbo.lua

    Takes a scene name as parameter, rendering it to an FBO and
    presenting that FBO on a quad in space.
]]
textured_fbo = {}

textured_fbo.__index = textured_fbo

function textured_fbo.new(...)
    local self = setmetatable({}, textured_fbo)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end
    return self
end

function textured_fbo:init(scene)
    self.vao = 0
    self.vbos = {}
    self.prog = 0
    self.texID = 0
    self.dataDir = nil
    self.clearColor = {.2,.2,.2,.0}
    self.glfont = nil

    if not scene then
      scene = require("scene.colorcube").new()
    end
    self.Scene = scene

    --TODO Model matrix
end

function textured_fbo:setDataDirectory(dir)
    self.dataDir = dir
end

local openGL = require("opengl")
local ffi = require("ffi")
local mm = require("util.matrixmath")
local sf = require("util.shaderfunctions")
local fbf = require("util.fbofunctions")
require("util.glfont")

local glIntv   = ffi.typeof('GLint[?]')
local glUintv  = ffi.typeof('GLuint[?]')
local glFloatv = ffi.typeof('GLfloat[?]')

local basic_vert = [[
#version 310 es

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

in vec4 vPosition;
in vec4 vColor;

out vec3 vfColor;

uniform mat4 mmtx;
uniform mat4 vmtx;
uniform mat4 prmtx;

void main()
{
    vfColor = vColor.xyz;
    gl_Position = prmtx * vmtx * mmtx * vPosition;
    //gl_Position.x *= 1.618;
}
]]

local basic_frag = [[
#version 310 es

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

in vec3 vfColor;
out vec4 fragColor;

uniform sampler2D sTex;

void main()
{
    vec4 tc = texture(sTex, vfColor.xy);
    fragColor = vec4(tc.xyz, 1.);
}
]]

function textured_fbo:setDataDirectory(dir)
    if self.Scene.setDataDirectory then
        self.Scene:setDataDirectory(dir)
    end
    self.dataDir = dir
end

function textured_fbo:init_cube_attributes()
    local v = {
        0,0,0,
        1,0,0,
        1,1,0,
        0,1,0,
    }
    local verts = glFloatv(#v,v)

    local c = {
        0,0, 1,0, 1,1, 0,1,
    }
    local cols = glFloatv(#c,c)

    local vpos_loc = gl.glGetAttribLocation(self.prog, "vPosition")
    local vcol_loc = gl.glGetAttribLocation(self.prog, "vColor")

    local vvbo = glIntv(0)
    gl.glGenBuffers(1, vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vpos_loc, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(self.vbos, vvbo)

    local cvbo = glIntv(0)
    gl.glGenBuffers(1, cvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, cvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(cols), cols, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vcol_loc, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(self.vbos, cvbo)

    gl.glEnableVertexAttribArray(vpos_loc)
    gl.glEnableVertexAttribArray(vcol_loc)

    local q = { -- Vertex indices for drawing triangles of faces
        0,3,2, 1,0,2,
    }
    local quads = glUintv(#q,q)
    local qvbo = glIntv(0)
    gl.glGenBuffers(1, qvbo)
    gl.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER, qvbo[0])
    gl.glBufferData(GL.GL_ELEMENT_ARRAY_BUFFER, ffi.sizeof(quads), quads, GL.GL_STATIC_DRAW)
    table.insert(self.vbos, qvbo)
end

function textured_fbo:initGL()
    local vaoId = ffi.new("int[1]")
    gl.glGenVertexArrays(1, vaoId)
    self.vao = vaoId[0]
    gl.glBindVertexArray(self.vao)

    self.prog = sf.make_shader_from_source({
        vsrc = basic_vert,
        fsrc = basic_frag,
        })

    self:init_cube_attributes()
    self.Scene:initGL()

    -- FBO init
    local sz = 800
    w,h = math.floor(
        --1.618*
        sz),sz
    --self.fbo:resize_fbo(w,h)
    self.fbo = fbf.allocate_fbo(w,h,true)
    gl.glBindVertexArray(0)

    dir = "fonts"
    if self.dataDir then dir = self.dataDir .. "/" .. dir end
    self.glfont = GLFont.new('segoe_ui128.fnt', 'segoe_ui128_0.raw')
    self.glfont:setDataDirectory(dir)
    self.glfont:initGL()
end

function textured_fbo:exitGL()
    gl.glBindVertexArray(self.vao)
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}

    gl.glDeleteProgram(self.prog)
    local texdel = ffi.new("GLuint[1]", self.texID)
    gl.glDeleteTextures(1,texdel)

    fbf.deallocate_fbo(self.fbo)
    self.Scene:exitGL()

    local vaoId = ffi.new("GLuint[1]", self.vao)
    gl.glDeleteVertexArrays(1, vaoId)
    gl.glBindVertexArray(0)
    self.glfont:exitGL()
end

function textured_fbo:renderCaption() 
    if not self.label then return end
    local col = {1, 1, 1}
    local m = {}
    local p = {}
    mm.make_identity_matrix(m)
    local s = 1
    mm.glh_translate(m, 30, 640, 0)
    mm.glh_scale(m, s, s, s)

    -- TODO getStringWidth and center text
    mm.glh_ortho(p, 0, 1200, 800, 0, -1, 1)
    gl.glDisable(GL.GL_DEPTH_TEST)
    self.glfont:render_string(m, p, col, self.label)

    gl.glEnable(GL.GL_DEPTH_TEST)
end

function textured_fbo:renderPrePass(model, view, proj)
    -- Save viewport dimensions
    -- TODO: this is very inefficient; store them manually, restore at caller
    local vp = ffi.new("GLuint[4]", 0,0,0,0)
    gl.glGetIntegerv(GL.GL_VIEWPORT, vp)
    -- Save bound FBO
    local boundfbo = ffi.new("int[1]")
    gl.glGetIntegerv(GL.GL_FRAMEBUFFER_BINDING, boundfbo)


    local p = {}
    local aspect = self.fbo.w / self.fbo.h
    mm.glh_perspective_rh(p, 60 * math.pi/180, aspect, .004, 500)

    -- Render pre-pass to fbo
    fbf.bind_fbo(self.fbo)
    gl.glViewport(0,0, self.fbo.w, self.fbo.h)

    local c = self.clearColor
    gl.glClearColor(c[1],c[2],c[3],c[4])
    gl.glClear(GL.GL_COLOR_BUFFER_BIT + GL.GL_DEPTH_BUFFER_BIT)
    if self.Scene.renderEye then
        self.Scene:renderEye(model, view, p)
    else
        self.Scene:render_for_one_eye(view, p)
    end

    self:renderCaption()

    fbf.unbind_fbo(self.fbo)
    -- TODO: restore VAO?


    -- Restore viewport
    gl.glViewport(vp[0],vp[1],vp[2],vp[3])
    -- Restore FBO binding
    if boundfbo[0] ~= 0 then
        gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, boundfbo[0])
    end
end

function textured_fbo:renderEye(model, view, proj)
    self.model_matrix = {}
    for i=1,16 do self.model_matrix[i] = model[i] end
    self:renderPrePass(model, view, proj)

    local um_loc = gl.glGetUniformLocation(self.prog, "mmtx")
    local uv_loc = gl.glGetUniformLocation(self.prog, "vmtx")
    local upr_loc = gl.glGetUniformLocation(self.prog, "prmtx")
    gl.glUseProgram(self.prog)
    gl.glUniformMatrix4fv(uv_loc, 1, GL.GL_FALSE, glFloatv(16, view))
    gl.glUniformMatrix4fv(upr_loc, 1, GL.GL_FALSE, glFloatv(16, proj))

    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.fbo.tex)
    local stex_loc = gl.glGetUniformLocation(self.prog, "sTex")
    gl.glUniform1i(stex_loc, 0)

    gl.glUniformMatrix4fv(um_loc, 1, GL.GL_FALSE, glFloatv(16, model))
    gl.glBindVertexArray(self.vao)
    gl.glDrawElements(GL.GL_TRIANGLES, 6*3*2, GL.GL_UNSIGNED_INT, nil)
    gl.glBindVertexArray(0)

    gl.glUseProgram(0)
end

function textured_fbo:timestep(absTime, dt)
    if self.Scene.timestep then
        self.Scene:timestep(absTime, dt)
    end
end

function textured_fbo:scaleFbo(fac)
    local w,h = math.floor(self.fbo.w*fac), math.floor(self.fbo.h*fac)
    fbf.deallocate_fbo(self.fbo)
    self.fbo = fbf.allocate_fbo(w,h,true)

    if true then
        self.label = tostring(w)..'x'..tostring(h)
    end
end

function textured_fbo:keypressed(key, scancode, action, mods)
    -- Maybe when ctrl is held?
    local scaleFactor = 1.2
    if key == 45 then -- -(minus)
        self:scaleFbo(1/scaleFactor)
        return true
    elseif key == 61 then -- +
        self:scaleFbo(scaleFactor)
        return true
    end

    if self.Scene.keypressed then
        return self.Scene:keypressed(key)
    end
end

function textured_fbo:onray(ro, rd)
    function rayTriangleIntersect(ro, rd, tripts)
        -- https://en.wikipedia.org/wiki/M%C3%B6ller%E2%80%93Trumbore_intersection_algorithm
        local EPSILON = .0000001
        local v0 = tripts[1]
        local v1 = tripts[2]
        local v2 = tripts[3]

        local edge1 = {}
        for i=1,3 do edge1[i] = v1[i] - v0[i] end
        local edge2 = {}
        for i=1,3 do edge2[i] = v2[i] - v0[i] end
        local h = mm.cross(rd, edge2)
        local a = mm.dot(edge1, h)
        if a > -EPSILON and a < EPSILON then
            return nil
        end
        local f = 1/a
        local s = {}
        for i=1,3 do s[i] = ro[i] - v0[i] end
        local u = f * mm.dot(s, h)
        if u < 0 or u > 1 then
            return nil
        end
        local q = mm.cross(s, edge1)
        local v = f * mm.dot(rd, q)
        if v < 0 or (u+v) > 1 then
            return nil
        end
        local t = f * mm.dot(edge2, q)
        if t > EPSILON then
            print("Hit t:",t)
            local hitpt = {}
            for i=1,3 do hitpt[i] = ro[i] + rd[i]*t end
            return hitpt
        else
            return nil
        end
    end

    local ar = 1 --.618
    local p0,p1,p2,p3 =
        {0,0,0,1},
        {ar,0,0,1},
        {0,1,0,1},
        {ar,1,0,1}

    ---@todo apply modelview matrix
    if self.model_matrix then
        local m = {}
        for i=1,16 do m[i] = self.model_matrix[i] end
        p0 = mm.transform(p0, m)
        p1 = mm.transform(p1, m)
        p2 = mm.transform(p2, m)
        p3 = mm.transform(p3, m)
    end

    local tripts = {p0,p1,p2,}
    local tripts2 = {p1,p2,p3,}
    local hitpt = rayTriangleIntersect(ro, rd, tripts)
    if not hitpt then
        hitpt = rayTriangleIntersect(ro, rd, tripts2)
    end
    if hitpt then

        local m = {}
        for i=1,16 do m[i] = self.model_matrix[i] end
        mm.affine_inverse(m)
        hitpt[4] = 1
        hitpt = mm.transform(hitpt, m)

        print("hitpt:",hitpt[1],hitpt[2],hitpt[3])
        return true
    end
    return false
end

return textured_fbo
