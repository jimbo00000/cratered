--[[ word.lua

    Check the translation of text in space
]]
word = {}
word.__index = word

function word.new(...)
    local self = setmetatable({}, word)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function word:init()
    self.glfont = nil
    self.dataDir = nil
    self.wordpos = {0,0,0}
    self.move = false
    self.word = "String"
end

require("util.glfont")
local mm = require("util.matrixmath")

function word:setDataDirectory(dir)
    self.dataDir = dir
end

function word:initGL()
    local dir = "fonts"
    if self.dataDir then dir = self.dataDir .. "/" .. dir end
    self.glfont = GLFont.new('courier_512.fnt', 'courier_512_0.raw')
    self.glfont:setDataDirectory(dir)
    self.glfont:initGL()
end

function word:exitGL()
    self.glfont:exitGL()
end

function word:renderEye(model, view, proj)
    local m = {}
    for i=1,16 do m[i] = model[i] end

    if self.move then
        mm.glh_translate(m, self.wordpos[1], self.wordpos[2], self.wordpos[3])
    end

    -- Center origin on upper-left corner
    mm.glh_translate(m, 0,.21,0)

    local s = .01
    mm.glh_scale(m, s, -s, s)    

    local col = {1, 1, 1}
    self.glfont:render_string2(m, view, proj, col, self.word)
end

function word:timestep(absTime, dt)
    local t = 3*absTime
    self.wordpos = {math.sin(t), 0, math.cos(t)}
end

function word:keypressed(key, scancode, action, mods)
    if action == 1 then
        self.move = not self.move
    end
end

return word
