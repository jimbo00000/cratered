-- graphics.lua

local mm = require("util.matrixmath")

local graphics = {}

-- Window info
local win_w = 800
local win_h = 600
local fb_w = win_w
local fb_h = win_h

local Scene = nil
local scenedir = "scene"
local scene_names = {
    "colorcube",
    'text_over_scene', --'starfield',
    'comets_in_space2',
    'space_scene',
    'text_terminal',
    'ships_in_space',
}
local scenes = {}

local Effect = nil
local effectdir = 'effect'
local effect_names = {
    'custom_effect',
    'crt_effect',
}
local effects = {}

local Camera = nil
local cameradir = 'util'
local camera_names = {
    'camera1',
    'camera5', -- follows a linear path
    'camera6', -- swings around the origin
    'camera1',
}
local cameras = {}

local bgBrightness = .3

-- Load  and call initGL on all scenes at startup
function graphics.initGL()
    for _,name in pairs(scene_names) do
        local fullname = scenedir..'.'..name
        local SceneLibrary = require(fullname)
        s = SceneLibrary.new()
        if s then
            if s.setDataDirectory then s:setDataDirectory("data") end
            s:initGL()
        end
        table.insert(scenes, s)
    end
    Scene = scenes[1]

    for _,name in pairs(effect_names) do
        local fullname = effectdir..'.'..name
        local EffectLibrary = require(fullname)
        e = EffectLibrary.new()
        if e then
            if e.setDataDirectory then e:setDataDirectory("data") end
            e:initGL(win_w,win_h)
            e:resize_fbo(win_w,win_h)
        end
        table.insert(effects, e)
    end
    Effect = nil

    for _,name in pairs(camera_names) do
        local fullname = cameradir..'.'..name
        local CameraLibrary = require(fullname)
        c = CameraLibrary.new()
        if c then
            if c.setDataDirectory then c:setDataDirectory("data") end
            c:init()
        end
        table.insert(cameras, c)
    end
    Camera = cameras[1]
end

function graphics.display()
    gl.glViewport(0,0, fb_w, fb_h)
    if Effect then Effect:bind_fbo() end

    local b = bgBrightness
    gl.glClearColor(b,b,b,0)
    gl.glClear(GL.GL_COLOR_BUFFER_BIT + GL.GL_DEPTH_BUFFER_BIT)
    gl.glEnable(GL.GL_DEPTH_TEST)

    if Scene and Camera then
        local m = {}
        mm.make_identity_matrix(m)
        if Camera.getModelMatrix then m = Camera:getModelMatrix() end

        local v = Camera:getViewMatrix()
        mm.affine_inverse(v)
        local p = {}
        local aspect = win_w / win_h
        mm.glh_perspective_rh(p, 60 * math.pi/180, aspect, .004, 500)

        if Scene.renderEye then Scene:renderEye(m,v,p) else
        Scene:render_for_one_eye(v,p) end
    end
    if Effect then Effect:unbind_fbo() end

    -- Effect post-processing and present
    if Effect then
        gl.glDisable(GL.GL_DEPTH_TEST)
        gl.glViewport(0,0, win_w, win_h)
        Effect:present(win_w, win_h)
    end

end

function graphics.resize(w, h)
    win_w, win_h = w, h
    fb_w, fb_h = win_w, win_h
    if Scene and Scene.resizeViewport then Scene:resizeViewport(w,h) end
    if Effect then Effect:resize_fbo(w,h) end
end

function graphics.timestep(absTime, dt)
    if Scene and Scene.timestep then Scene:timestep(absTime, dt) end
    if Effect and Effect.timestep then Effect:timestep(absTime, dt) end
end

function graphics.setbpm(bpm)
    Scene.BPM = bpm
end

-- A table of handlers for different track name-value pairs coming from
-- the rocket module. Values may be updated by messages from the editor.
-- Keys must match track names sent to editor.

-- This table gets evaluated first.
graphics.sync_precallbacks = {
    ["Scene"] = function(v)
        -- Switch scenes with an index
        if scenes[v] then Scene = scenes[v] end
    end,
    ["Effect"] = function(v)
        local oldEffect = Effect
        Effect = effects[v]
        if Effect and oldEffect ~= Effect then
            Effect:resize_fbo(win_w,win_h)
        end
    end,
    ["Camera"] = function(v)
        if cameras[v] then Camera = cameras[v] end
    end,
}

graphics.sync_callbacks = {
    ["cam1:rotx"] = function(v)
        if Camera.camrot then Camera.camrot[1] = v end
    end,

    ["cam2:approach_dist"] = function(v)
        if Camera.approach_dist then Camera.approach_dist = v end
    end,

    ["cam3:swing_angle"] = function(v)
        if Camera.swing_angle then Camera.swing_angle = v end
    end,

    ["cam:rotx"] = function(v)
        if Camera.camrot then Camera.camrot[1] = v end
    end,
    ["cam:roty"] = function(v)
        if Camera.camrot then Camera.camrot[2] = v end
    end,
    ["cam:posx"] = function(v)
        if Camera.chassis then Camera.chassis[1] = v end
    end,
    ["cam:posy"] = function(v)
        if Camera.chassis then Camera.chassis[2] = v end
    end,
    ["cam:posz"] = function(v)
        if Camera.chassis then Camera.chassis[3] = v end
    end,

    ["comets:reset_flag"] = function(v)
        if Scene.reset_flag then Scene.reset_flag = v end
    end,

    ["comets:stars_vel"] = function(v)
        if Scene.xvel then Scene.xvel = v end
        if Scene.stars and Scene.stars.xvel then Scene.stars.xvel = v end
    end,

    ["comets:comet_vel"] = function(v)
        if Scene.comet_vel then Scene.comet_vel = v end
    end,

    ["term:comet_strength"] = function(v)
        if Scene.comet_strength then Scene.comet_strength = v end
    end,

    ["meteor:launch_flag"] = function(v)
        if Scene.launch_flag then Scene.launch_flag = v end
    end,

    ["term:text_idx"] = function(v)
        if Scene.text_idx then Scene.text_idx = v end
    end,

    ["bgBright"] = function(v)
        if bgBrightness then bgBrightness = v end
    end,

    ["showtext"] = function(v)
        if Scene.showtext then Scene.showtext = (v ~= 0) end
    end,

    ["tri:col"] = function(v)
        if Scene.col then Scene.col = v end
        if Scene.time then Scene.time = 6*v end
    end,

    -- Add new keyframe names here
}

function graphics.sync_params(get_param_value_at_current_time)
    -- The get_param_value_at_current_time function
    -- calls into rocket's track list of keyframes
    -- with the current time(according to main) as a parameter.
    local f = get_param_value_at_current_time
    if not f then return end

    local cblists = {
        graphics.sync_precallbacks,
        graphics.sync_callbacks,
    }
    for _,cbs in ipairs(cblists) do
        for trackname,_ in pairs(cbs) do
            local val = f(trackname)
            local g = cbs[trackname]
            if g and val then g(val) end
        end
    end
end

return graphics
