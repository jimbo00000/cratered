--[[ starfield.lua

]]
starfield = {}

starfield.__index = starfield

function starfield.new(...)
    local self = setmetatable({}, starfield)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function starfield:init()
    self.vbos = {}
    self.vao = 0
    self.progs = {}
    self.numPoints = 1024 * 8--*1024
    self.time = 0

    self.xvel = .02
end

local openGL = require("opengl")
local ffi = require("ffi")
local mm = require("util.matrixmath")
local sf = require("util.shaderfunctions")

local glIntv     = ffi.typeof('GLint[?]')
local glUintv    = ffi.typeof('GLuint[?]')
local glFloatv   = ffi.typeof('GLfloat[?]')

--[[
    Drawing instanced cubes: simple color per-vertex.
]]
local per_vertex_color_vert = [[
#version 330

in vec4 vPosition;
in vec4 vNormal;
in vec4 instancePosition;
in vec4 instanceOrientation;
in vec4 instanceColor;

out vec3 vfColor;

uniform mat4 mmtx;
uniform mat4 vmtx;
uniform mat4 prmtx;
uniform float instScale;

// Quaternion rotation of a vector
vec3 qtransform(vec4 q, vec3 v)
{
    return v + 2.0*cross(cross(v, q.xyz) + q.w*v, q.xyz);
}

void main()
{
    vec3 pos = vPosition.xyz;
    vec4 q = normalize(instanceOrientation);
    pos = qtransform(q, pos);
    pos *= instScale;
    pos += instancePosition.xyz;
    vfColor = instanceColor.rgb;
    gl_Position = prmtx * vmtx * mmtx * vec4(pos, 1.);
}
]]

local per_vertex_color_frag = [[
#version 330

in vec3 vfColor;
out vec4 fragColor;

void main()
{
    fragColor = vec4(vfColor, 1.);
}
]]

function starfield:setDataDirectory(dir)
    self.data_dir = dir
end

-- A simple cube mesh with per-vertex colors.
function starfield:init_single_instance_attributes()
    self:init_single_instance_attributes_from_array()
end

function starfield:init_single_instance_attributes_from_array()
    local verts = {
        0,0,0,
        1,0,0,
        1,1,0,
        0,1,0,

        0,0,1,
        1,0,1,
        1,1,1,
        0,1,1,

        0,0,0,
        1,0,0,
        1,0,1,
        0,0,1,

        0,1,0,
        1,1,0,
        1,1,1,
        0,1,1,

        0,0,0,
        0,1,0,
        0,1,1,
        0,0,1,

        1,0,0,
        1,1,0,
        1,1,1,
        1,0,1,
    }
    for i=1,#verts do verts[i] = verts[i]-.5 end
    local vertsArray = glFloatv(#verts, verts)

    local prog = self.progs.color
    local vpos_loc = gl.glGetAttribLocation(prog, "vPosition")

    local vvbo = glIntv(0)
    gl.glGenBuffers(1, vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(vertsArray), vertsArray, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vpos_loc, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(self.vbos, vvbo)

    gl.glEnableVertexAttribArray(vpos_loc)

    local tris = {
        0,3,2, 1,0,2,
        4,5,6, 7,4,6,

        8,11,10, 9,8,10,
        12,13,14, 15,12,14,

        16,19,18, 17,16,18,
        20,21,22, 23,20,22,
    }
    local trisArray = glUintv(#tris, tris)
    local qvbo = glIntv(0)
    gl.glGenBuffers(1, qvbo)
    gl.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER, qvbo[0])
    gl.glBufferData(GL.GL_ELEMENT_ARRAY_BUFFER, ffi.sizeof(trisArray), trisArray, GL.GL_STATIC_DRAW)
    table.insert(self.vbos, qvbo)

    self.numInstTris = #tris
end


--[[
    Scatter instance positions randomly in starfield array.
]]
local randomize_instance_positions_comp_src = [[
#version 430

layout(local_size_x=256) in;
layout(std430, binding=0) buffer pblock { vec4 positions[]; };
layout(std430, binding=1) buffer oblock { vec4 orientations[]; };
layout(std430, binding=2) buffer vblock { vec4 velocities[]; };
layout(std430, binding=3) buffer avblock { vec4 angularVelocities[]; };
layout(std430, binding=4) buffer cblock { vec4 colors[]; };
uniform int numPoints;

float hash( float n ) { return fract(sin(n)*43758.5453); }

// by Shane
// https://www.shadertoy.com/view/MlcGD7
vec3 firePalette(float i)
{
    float T = 1400. + 1400.*i; // Temperature range (in Kelvin).
    vec3 L = vec3(7.4, 5.6, 4.4); // Red, green, blue wavelengths (in hundreds of nanometers).
    L = pow(L,vec3(5.0)) * (exp(1.43876719683e5/(T*L))-1.0);
    return 1.0-exp(-5e8/L); // Exposure level. Set to "50." For "70," change the "5" to a "7," etc.
}

void main()
{
    uint index = gl_GlobalInvocationID.x;
    if (index >= numPoints)
        return;
    float fi = float(index+1) / float(numPoints);

    positions[index] = vec4(hash(fi*2.3), hash(fi*4.1), hash(fi*5.3), 1.);

    orientations[index] = vec4(hash(fi*4.3), hash(fi*2.9), hash(fi*7.), hash(fi*4.7));

    vec3 randv = vec3(hash(fi*4.9), hash(fi*1.9), hash(fi*3.7));
    vec3 motiondir = 2.*(vec3(-.5) + randv); // isotropic, go in all directions
    motiondir = pow(length(motiondir), 7.) * normalize(motiondir);
    velocities[index] = .01* vec4(.05*motiondir, 0.);

    vec3 randav = vec3(hash(fi*6.1), hash(fi*5.9), hash(fi*3.1));
    vec3 rotangle = 2.*(vec3(-.5) + randav); // isotropic, go in all directions
    angularVelocities[index] = (vec4(rotangle, 3000.*hash(fi*0.9)));

    vec3 randcol = firePalette(.03+abs(hash(fi*0.7)));
    colors[index] = vec4(randcol, 1.);
}
]]
function starfield:randomize_instance_positions()
    if self.progs.scatter_instances == nil then
        self.progs.scatter_instances = sf.make_shader_from_source({
            compsrc = randomize_instance_positions_comp_src,
            })
    end
    local pvbo = self.vbos.inst_positions
    local ovbo = self.vbos.inst_orientations
    local vvbo = self.vbos.inst_velocities
    local avvbo = self.vbos.inst_angularvelocities
    local cvbo = self.vbos.inst_colors
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 0, pvbo[0])
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 1, ovbo[0])
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 2, vvbo[0])
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 3, avvbo[0])
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 4, cvbo[0])
    local prog = self.progs.scatter_instances
    local function uni(name) return gl.glGetUniformLocation(prog, name) end
    gl.glUseProgram(prog)
    gl.glUniform1i(uni("numPoints"), self.numPoints)
    gl.glDispatchCompute(self.numPoints/256+1, 1, 1)
    gl.glUseProgram(0)
end

--[[
    Apply angular velocity every timestep
]]
local instance_timestep_comp_src = [[
#version 430

layout(local_size_x=256) in;
layout(std430, binding=0) buffer pblock { vec4 positions[]; };
layout(std430, binding=1) buffer oblock { vec4 orientations[]; };
layout(std430, binding=2) buffer vblock { vec4 velocities[]; };
layout(std430, binding=3) buffer avblock { vec4 angularVelocities[]; };
uniform int numPoints;
uniform float dt;
uniform float xvel;

// http://www.gamedev.net/reference/articles/article1095.asp
//w = w1w2 - x1x2 - y1y2 - z1z2
//x = w1x2 + x1w2 + y1z2 - z1y2
//y = w1y2 + y1w2 + z1x2 - x1z2
//z = w1z2 + z1w2 + x1y2 - y1x2
// Remember the w coordinate is last!
vec4 qconcat3(vec4 a, vec4 b)
{
    return vec4(
        a.w*b.x + a.x*b.w + a.y*b.z - a.z*b.y,
        a.w*b.y + a.y*b.w + a.z*b.x - a.x*b.z,
        a.w*b.z + a.z*b.w + a.x*b.y - a.y*b.x,
        a.w*b.w - a.x*b.x - a.y*b.y - a.z*b.z
        );
}

void main()
{
    uint index = gl_GlobalInvocationID.x;
    if (index >= numPoints)
        return;

    // Position
    vec4 p = positions[index];
    vec3 v = velocities[index].xyz;
    p.xyz += dt * v.xyz;

    p.x += dt * xvel;
    if (p.x < 0.)
        p.x += 1.;
    if (p.x > 1.)
        p.x -= 1.;

    positions[index] = p;
    velocities[index].xyz = v;

    // Rotation
    vec4 q = orientations[index];
    q = normalize(qconcat3(q, dt * angularVelocities[index]));
    orientations[index] = q;
}
]]
function starfield:timestep_instances(dt)
    if self.progs.step_instances == nil then
        self.progs.step_instances = sf.make_shader_from_source({
            compsrc = instance_timestep_comp_src,
            })
    end
    local prog = self.progs.step_instances
    local function uni(name) return gl.glGetUniformLocation(prog, name) end

    local pvbo = self.vbos.inst_positions
    local ovbo = self.vbos.inst_orientations
    local vvbo = self.vbos.inst_velocities
    local avvbo = self.vbos.inst_angularvelocities
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 0, pvbo[0])
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 1, ovbo[0])
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 2, vvbo[0])
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 3, avvbo[0])
    gl.glUseProgram(prog)
    gl.glUniform1i(uni("numPoints"), self.numPoints)
    gl.glUniform1f(uni("dt"), dt)
    local xvel = self.xvel or 0
    gl.glUniform1f(uni("xvel"), xvel)
    gl.glDispatchCompute(self.numPoints/256+1, 1, 1)
    gl.glUseProgram(0)
end

function starfield:init_per_instance_attributes()
    local prog = self.progs.color
    local sz = 4 * self.numPoints * ffi.sizeof('GLfloat') -- xyzw

    local insp_loc = gl.glGetAttribLocation(prog, "instancePosition")
    local ipvbo = glIntv(0)
    gl.glGenBuffers(1, ipvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, ipvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, sz, nil, GL.GL_STATIC_COPY)
    gl.glVertexAttribPointer(insp_loc, 4, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    self.vbos.inst_positions = ipvbo
    gl.glVertexAttribDivisor(insp_loc, 1)
    gl.glEnableVertexAttribArray(insp_loc)

    local inso_loc = gl.glGetAttribLocation(prog, "instanceOrientation")
    local iovbo = glIntv(0)
    gl.glGenBuffers(1, iovbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, iovbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, sz, nil, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(inso_loc, 4, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    self.vbos.inst_orientations = iovbo
    gl.glVertexAttribDivisor(inso_loc, 1)
    gl.glEnableVertexAttribArray(inso_loc)

    -- Velocities and angular velocities are not drawn; used only by compute.
    local vvbo = glIntv(0)
    gl.glGenBuffers(1, vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, sz, nil, GL.GL_STATIC_DRAW)
    self.vbos.inst_velocities = vvbo

    -- Angular velocities are not drawn; used only by compute.
    local avvbo = glIntv(0)
    gl.glGenBuffers(1, avvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, avvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, sz, nil, GL.GL_STATIC_DRAW)
    self.vbos.inst_angularvelocities = avvbo

    local insc_loc = gl.glGetAttribLocation(prog, "instanceColor")
    local icvbo = glIntv(0)
    gl.glGenBuffers(1, icvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, icvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, sz, nil, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(insc_loc, 4, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    self.vbos.inst_colors = icvbo
    gl.glVertexAttribDivisor(insc_loc, 1)
    gl.glEnableVertexAttribArray(insc_loc)

end

function starfield:initGL()
    self.progs.color = sf.make_shader_from_source({
        vsrc = per_vertex_color_vert,
        fsrc = per_vertex_color_frag,
        })

    local vaoId = ffi.new("int[1]")
    gl.glGenVertexArrays(1, vaoId)
    self.vao = vaoId[0]
    gl.glBindVertexArray(self.vao)
    do
        self:init_single_instance_attributes()
        self:init_per_instance_attributes()
        self:randomize_instance_positions()
    end

    gl.glBindVertexArray(0)
end

function starfield:exitGL()
    for _,p in pairs(self.progs) do
        gl.glDeleteProgram(p)
    end
    self.progs = {}

    gl.glBindVertexArray(self.vao)
    do
        for _,v in pairs(self.vbos) do
            gl.glDeleteBuffers(1,v)
        end
        self.vbos = {}
    end
    gl.glBindVertexArray(0)
    local vaoId = ffi.new("GLuint[1]", self.vao)
    gl.glDeleteVertexArrays(1, vaoId)
end

function starfield:renderEye(model, view, proj)
    local prog = self.progs.color
    local function uni(name) return gl.glGetUniformLocation(prog, name) end
    gl.glUseProgram(prog)
    gl.glUniformMatrix4fv(uni("prmtx"), 1, GL.GL_FALSE, glFloatv(16, proj))
    
    local SZ = 10

    local m = {}
    for i=1,16 do m[i] = model[i] end
    local s = 10 * SZ
    mm.glh_scale(m, s,s,s)
    mm.glh_translate(m, -.5, -.5, -.5)

    gl.glUniformMatrix4fv(uni("mmtx"), 1, GL.GL_FALSE, glFloatv(16, m))
    gl.glUniformMatrix4fv(uni("vmtx"), 1, GL.GL_FALSE, glFloatv(16, view))

    local sc = .01 / math.pow(self.numPoints,1/3)
    gl.glUniform1f(uni("instScale"), sc)

    gl.glBindVertexArray(self.vao)
    gl.glDrawElementsInstanced(GL.GL_TRIANGLES, self.numInstTris, GL.GL_UNSIGNED_INT, nil, self.numPoints)
    gl.glBindVertexArray(0)
    gl.glUseProgram(0)
end

function starfield:timestep(absTime, dt)
    self:timestep_instances(dt+.000000001)
end

function starfield:rescaleInstances()
    --print(self.numPoints)
    gl.glBindVertexArray(self.vao)
    do
        gl.glDeleteBuffers(1,self.vbos.inst_positions)
        gl.glDeleteBuffers(1,self.vbos.inst_orientations)
        gl.glDeleteBuffers(1,self.vbos.inst_velocities)
        gl.glDeleteBuffers(1,self.vbos.inst_angularvelocities)
        gl.glDeleteBuffers(1,self.vbos.inst_colors)

        self:init_per_instance_attributes()
        self:randomize_instance_positions()
    end
    gl.glBindVertexArray(0)
end

function starfield:keypressed(key, scancode, action, mods)

    if key == string.byte('-') then
        self.numPoints = self.numPoints / 2
        self:rescaleInstances()
        return true
    elseif key == string.byte('=') then
        self.numPoints = self.numPoints * 2
        self:rescaleInstances()
        return true
    end

    local factor = 1.2
    if key == string.byte('[') then
        self.xvel = self.xvel / factor
        print(self.xvel)
        return true
    elseif key == string.byte(']') then
        self.xvel = self.xvel * factor
        print(self.xvel)
        return true
    end
    return false
end

return starfield
