--[[ coma.lua

    The fuzzy ring around a comet

    Uses gridcube_scene.lua to create a fine subdivided mesh topologically
    homeomorphic to a sphere(starts life as a cube), then applies a compute
    shader on vertex positions to cumulatively deform the mesh.
]]
coma = {}
coma.__index = coma

function coma.new(...)
    local self = setmetatable({}, coma)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

local GridLib = require("scene.gridcube")
local ffi = require("ffi")
local mm = require("util.matrixmath")
local sf = require("util.shaderfunctions")

local glIntv   = ffi.typeof('GLint[?]')
local glFloatv = ffi.typeof('GLfloat[?]')

function coma:init(subdivs)
    self.progs = {}
    self.grid = GridLib.new(subdivs or 64)
    self.time = 0
end

--[[
    Perturb all vertices with a function.
]]
local coma_perturbverts_comp_src = [[
#version 310 es
#line 22
layout(local_size_x=128) in;
layout(std430, binding=0) buffer pblock { vec4 positions[]; };

vec3 displacePoint(vec3 p)
{
    vec3 center = vec3(.5);
    float radius = 1.;
    vec3 pt = center + radius*normalize(p-center)
        //+ vec3(0.,0.,p.z*p.z)
        ;
    pt.z *= 1.618; //abs(pt.z);
    return pt;
}

void main()
{
    uint index = gl_GlobalInvocationID.x;
    vec4 p = positions[index];

    p.xyz = displacePoint(p.xyz);

    positions[index] = p;
}
]]

function coma:perturbVertexPositions()
    local vvbo = self.grid:get_vertices_vbo()
    local num_verts = self.grid:get_num_verts()
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 0, vvbo[0])
    local prog = self.progs.perturbverts
    gl.glUseProgram(prog)

    gl.glDispatchCompute(num_verts/128+1, 1, 1)
    gl.glUseProgram(0)
end

local coma_vert = [[
#version 310 es

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

in vec4 vPosition;
in vec4 vNormal;
in vec2 vTexCoord;

out vec3 vfWorldPos;
out vec3 vfNormal;
out vec2 vfTexCoord;

uniform mat4 modelmtx;
uniform mat4 viewmtx;
uniform mat4 projmtx;

void main()
{
    mat4 mvmtx = viewmtx * modelmtx;
    vfWorldPos = (vPosition).xyz;
    vfNormal = normalize(mat3(modelmtx) * vNormal.xyz);
    vfTexCoord = vTexCoord;
    gl_Position = projmtx *  mvmtx * vPosition;
}
]]

local coma_frag = [[
#version 310 es

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

uniform mat4 modelmtx;
uniform mat4 viewmtx;
uniform float time;

in vec3 vfWorldPos;
in vec3 vfNormal;
in vec2 vfTexCoord;
out vec4 fragColor;
#line 122
#define PI 3.1415926535897932384626433832795
float atan2(in float y, in float x)
{
    return x == 0.0 ? sign(y)*PI/2. : atan(y, x);
}

void main()
{
    vec3 fwd = inverse(mat3(viewmtx)) * vec3(0.,0.,-1.);

    float dp = dot(vfNormal, fwd);
    float zp = max(0.,1.5-vfWorldPos.z);

    vec3 col = vec3(vfTexCoord,1.);
        //normalize(abs(vfNormal)), 
        //vec3(zp,0.,1.),
        //vec3(1.-dp),


    // https://www.bidouille.org/prog/plasma
    float v = 0.0;
    float u_k = 20.;
    vec2 tc = vfTexCoord;

    tc.x = vfWorldPos.z - 1.3*time;
    tc.y = atan2(vfTexCoord.y, vfTexCoord.x);

    vec2 c = tc * u_k - u_k/2.0;
    v += sin((c.x+time));
    v += sin((c.y+time)/2.0);
    v += sin((c.x+c.y+time)/2.0);
    c += u_k/2.0 * vec2(sin(time/3.0), cos(time/2.0));
    v += sin(sqrt(c.x*c.x+c.y*c.y+1.0)+time);
    v = v/2.0;
    
    //col = vec3(v);
    col = vec3(1, sin(PI*v), cos(PI*v));


    fragColor = vec4(
        col,
        pow(1.-dp, 3.) * zp
        );
}
]]

function coma:initGL()
    self.grid:initGL()

    self.progs.perturbverts= sf.make_shader_from_source({
        compsrc = coma_perturbverts_comp_src,
        })

    self.progs.drawshader = sf.make_shader_from_source({
        vsrc = coma_vert,
        fsrc = coma_frag,
        })

    gl.glBindVertexArray(self.grid.vao)
    self:perturbVertexPositions()
    self.grid:recalc_normals()

    gl.glBindVertexArray(0)
end

function coma:exitGL()
    self.grid:exitGL()
    for _,v in pairs(self.progs) do
        gl.glDeleteProgram(v)
    end
end

function coma:renderEye(model, view, proj)
    gl.glEnable(GL.GL_CULL_FACE)
    gl.glEnable(GL.GL_DEPTH_TEST)
    gl.glEnable(GL.GL_BLEND)

    local prog = self.progs.drawshader
    local um_loc = gl.glGetUniformLocation(prog, "modelmtx")
    local uv_loc = gl.glGetUniformLocation(prog, "viewmtx")
    local up_loc = gl.glGetUniformLocation(prog, "projmtx")
    gl.glUseProgram(prog)
    --gl.glUniformMatrix4fv(um_loc, 1, GL.GL_FALSE, glFloatv(16, model))
    gl.glUniformMatrix4fv(uv_loc, 1, GL.GL_FALSE, glFloatv(16, view))
    gl.glUniformMatrix4fv(up_loc, 1, GL.GL_FALSE, glFloatv(16, proj))

    -- try to add some variation
    local time = self.time
    if self.timescale then
        self.time = self.time * self.timescale
    end
    if self.phase then
        self.time = self.time + self.phase
    end

    local ut_loc = gl.glGetUniformLocation(prog, "time")
    gl.glUniform1f(ut_loc, time)

    local m = {}
    mm.make_identity_matrix(m)
    for i=1,16 do m[i] = model[i] end
    local s = .5
    --mm.glh_scale(m,s,s,s)
    --mm.glh_rotate(m, 10*self.time , .1,.7,.3)
    mm.glh_translate(m, -.5, -.5, -.5)
    gl.glUniformMatrix4fv(um_loc, 1, GL.GL_FALSE, glFloatv(16, m))
    gl.glBindVertexArray(self.grid.vao)
    --gl.glPolygonMode(GL.GL_FRONT_AND_BACK, GL.GL_LINE)
    gl.glDrawElements(GL.GL_TRIANGLES,
        self.grid.cubemesh.num_tri_idxs,
        GL.GL_UNSIGNED_INT, nil)
    gl.glBindVertexArray(0)

    gl.glUseProgram(0)

    self.model_matrix = model

    --gl.glDisable(GL.GL_BLEND)
end

function coma:timestep(absTime, dt)
    self.grid:timestep(absTime, dt)

    -- TODO
    local timescale = self.timescale or 1
    self.time = absTime
end

function coma:keypressed(ch)
end

function coma:onSingleTouch(pointerid, action, x, y)
end

return coma
