--[[ meteorite.lua

    One scene orbiting another scene.
]]
meteorite = {}

meteorite.__index = meteorite

function meteorite.new(...)
    local self = setmetatable({}, meteorite)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

local MoonLib = require("scene.moon")
local CometLib = require("scene.comet")
local EjectaLib = require("scene.ejecta")
--local OriginLib = require("scene.origin")
local mm = require("util.matrixmath")


local charset = {}  do -- [0-9a-zA-Z]
    for c = 48, 57  do table.insert(charset, string.char(c)) end
    for c = 65, 90  do table.insert(charset, string.char(c)) end
    for c = 97, 122 do table.insert(charset, string.char(c)) end
end

local function randomString(length)
    if not length or length <= 0 then return '' end
    math.randomseed(os.clock()^5)
    return randomString(length - 1) .. charset[math.random(1, #charset)]
end


function meteorite:init()
    self.numLights = 8
    self.orbiter_count = 8

    self.planet = MoonLib.new(256, self.numLights)
    self.orbiters = {}
    self.ejectas = {}
    for i=1,self.orbiter_count do
        local sc = CometLib.new(16)
        table.insert(self.orbiters, sc)
        table.insert(self.ejectas, EjectaLib.new(128, self.numLights))
    end
    self.ejec_idx = 1
    self.sunpos = {5,5,5}

    if OriginLib then self.origin = OriginLib.new() end

    self.recentimpacts = {}

    --self.timescale = .2
    self.stringbuf = ''
end

function meteorite:setDataDirectory(dir)
    if self.planet.setDataDirectory then self.planet:setDataDirectory(dir) end
    for k,v in pairs(self.orbiters) do
        if v.setDataDirectory then v:setDataDirectory(dir) end
    end
    for k,v in pairs(self.ejectas) do
        if v.setDataDirectory then v:setDataDirectory(dir) end
    end
end

function meteorite:initGL()
    self.planet:initGL()
    for k,v in pairs(self.orbiters) do
        v:initGL()
    end
    for k,v in pairs(self.ejectas) do
        v:initGL()
    end
    if self.origin then self.origin:initGL() end
end

function meteorite:exitGL()
    self.planet:exitGL()
    for k,v in pairs(self.orbiters) do
        v:exitGL()
    end
    for k,v in pairs(self.ejectas) do
        v:exitGL()
    end
    if self.origin then self.origin:exitGL() end
end

function meteorite:renderMeteor(meteor_scene, model, view, proj)
    local meteor_data = meteor_scene.meteor_data
    if not meteor_data then return end

    -- Construct basis to point coma at target
    local mb = {}
    for i=1,16 do mb[i] = model[i] end
    mm.glh_translate(mb, meteor_data.pos[1], meteor_data.pos[2], meteor_data.pos[3])

    local side = {0,0,-1,0}
    local fwd = {}
    for i=1,3 do
        fwd[i] = -(meteor_data.endPos[i] - meteor_data.startPos[i])
    end
    mm.normalize(fwd)
    local upv = mm.cross(fwd, side)
    mm.normalize(upv)
    side = mm.cross(upv, fwd)

    local cometrot = {}
    mm.make_identity_matrix(cometrot)
    for i=1,3 do
        cometrot[i] = fwd[i]
        cometrot[i+4] = side[i]
        cometrot[i+8] = upv[i]
    end
    mm.post_multiply(mb, cometrot)

    local sz = .2 * meteor_data.strength
    sz = math.min(sz, .4)
    mm.glh_scale(mb, sz,sz,sz)
    meteor_scene:renderEye(mb, view, proj)
end

function meteorite:renderEye(model, view, proj)
    local m = {}
    for i=1,16 do m[i] = model[i] end
    self.planet:renderEye(m, view, proj)
    for k,v in pairs(self.ejectas) do
        v:renderEye(m, view, proj)
    end
    for k,v in pairs(self.orbiters) do
        self:renderMeteor(v, model, view, proj)
    end
    -- cache this
    self.model_matrix = {}
    for i=1,16 do self.model_matrix[i] = model[i] end
end

function meteorite:timestep(absTime, dt)
    if self.planet.timestep then self.planet:timestep(absTime, dt) end
    for k,v in pairs(self.ejectas) do
        v.timescale = self.timescale
        if v.timestep then v:timestep(absTime, dt) end
    end
    self.time = absTime

    for k,v in pairs(self.orbiters) do
        self:timestepMeteor(v, absTime)
    end

    do
        -- Assign lighting data
        local x,y,z = self.sunpos[1], self.sunpos[2], self.sunpos[3]
        local c = {0,0,0}
        local dir = {c[1]-x, c[2]-y, c[2]-z}
        mm.normalize(dir)
        self.lightData = {
            -- Light 1
            {
                x,y,z,0,
                dir[1], dir[2], dir[3],0,
                .85, .85, 1, 0,
                .1,.1,0,0, -- NOTE: std140 layout adds padding to 16-byte boundaries
            },
        }

        local explduration = 1 -- seconds
        local timescale = self.timescale or 1
        local scaled_duration = explduration / timescale
        --print(#self.recentimpacts)
        for i=1,#self.recentimpacts do
            if self.recentimpacts[i] then
                local age = absTime - self.recentimpacts[i][1]
                local hitpt = self.recentimpacts[i][2]
                hitpt[4] = 1
                hitpt = mm.transform(hitpt, self.model_matrix)

                local radscale = 1.1
                lightpt = {}
                for j=1,3 do lightpt[j] = radscale*hitpt[j] end

                -- Fade explosion color
                local explcol = {1, .5, 0} -- orange
                local colscale = 1-(age / scaled_duration)
                colscale = 1.8 * colscale -- light boost
                for c=1,3 do
                    explcol[c] = explcol[c] * colscale
                end

                local expl = {
                    lightpt[1], lightpt[2], lightpt[3],0,
                    -lightpt[1], -lightpt[2], -lightpt[3],0,
                    explcol[1], explcol[2], explcol[3], 0,
                    0,0,0,0,
                }
                table.insert(self.lightData, expl)
                if age > scaled_duration then
                    table.remove(self.recentimpacts, i)
                end
            end
        end

        local nolight = {
            0,0,0,0,
            0,0,0,0,
            0,0,0,0,
            0,0,0,0,
        }
        local offlights = self.numLights - #self.lightData
        --print('add lghts: ',offlights)

        for i=1,offlights do
            table.insert(self.lightData, nolight)
        end

        if #self.lightData == self.numLights then
            for k,v in pairs(self.ejectas) do
                v.lightData = self.lightData
            end
            self.planet.grid.lightData = self.lightData
        end
    end
end

function meteorite:charkeypressed(ch)
    self.stringbuf = self.stringbuf..ch
end

function meteorite:keypressed(key, scancode, action, mods)
    if action == 1 then
        if key == 257 then -- enter
            local hitpt = {
                math.random(),
                -1+2*math.random(),
                math.random(),
            }
            mm.normalize(hitpt)

            local txfm = false
            if txfm and self.model_matrix then
                local invmm = {}
                for i=1,16 do invmm[i] = self.model_matrix[i] end
                mm.affine_inverse(invmm)
                local hitpt4 = {hitpt[1], hitpt[2], hitpt[3], 1}
                hitpt4 = mm.transform(hitpt4, invmm)
                for i=1,3 do hitpt[i] = hitpt4[i] end
            end

            local word = self.stringbuf
            if string.len(word) == 0 then
                word = randomString(6)
            end
            self.stringbuf = ''

            self:launchMeteor(hitpt, 1, 1, word)
            return
        end

        if key >= string.byte('A') and key <= string.byte('Z') then
            --self.stringbuf = self.stringbuf..string.char(key)
            --return
        end
    end

    local ret = self.planet:keypressed(key, scancode, action, mods)
end

function meteorite:spawnEjecta(ejecta_scene, hitpt, numEjecPts)
    local sz = .2 * .5
    local hps = {}
    for i=1,3 do hps[i] = hitpt[i] * sz end
    ejecta_scene.startPos = {hps[1],hps[2],hps[3]}

    local center = {0,0,0}
    local dir = {}
    for i=1,3 do dir[i] = hitpt[i]-center[i] end
    mm.normalize(dir)
    local v = .5
    ejecta_scene.startDir = {v*dir[1], v*dir[2], v*dir[3]}
    local g = -.3
    ejecta_scene.accel = {g*dir[1], g*dir[2], g*dir[3]}

    ejecta_scene.numPoints = numEjecPts
    ejecta_scene:fire()
end

function meteorite:launchMeteor(hp, duration, strength, word)
    duration = duration or 1
    strength = strength or 1
    word = word or "meteor"

    local meteor = {}
    meteor.startTime = self.time
    meteor.endTime = self.time + duration
    meteor.startPos = {}
    for i=1,3 do
        meteor.startPos[i] = 4 * hp[i]
    end
    meteor.endPos = {}
    meteor.pos = {}
    for i=1,3 do
        meteor.endPos[i] = hp[i]
        meteor.pos[i] = meteor.startPos[i]
    end
    meteor.strength = strength

    -- Find an available meteor in the pool
    for k,v in pairs(self.orbiters) do
        if not v.meteor_data then
            v.meteor_data = meteor
            v.core.word = word
            break
        end
    end
end

function meteorite:timestepMeteor(meteor_scene, absTime)
    local meteor_data = meteor_scene.meteor_data
    if not meteor_data then return end

    if meteor_scene.timestep then meteor_scene:timestep(absTime, dt) end

    local tparam = (absTime - meteor_data.startTime) /
        (meteor_data.endTime - meteor_data.startTime)

    for i=1,3 do
        meteor_data.pos[i] = meteor_data.startPos[i] + tparam *
            (meteor_data.endPos[i] - meteor_data.startPos[i])
    end

    -- Hit condition
    if tparam >= .9 then
        self.planet:createCraterAtHitPoint(meteor_data.endPos, meteor_data.strength)

        -- Pool of ejecta scenes
        -- TODO: add start time and set as inactive after some time.
        local ej = self.ejectas[self.ejec_idx]

        -- log scale
        local numEjecPts = .25*math.pow(2, 10*meteor_data.strength)
        numEjecPts = math.min(numEjecPts, 4096)

        self:spawnEjecta(ej, meteor_data.endPos, numEjecPts)
        -- Advance idx
        self.ejec_idx = self.ejec_idx + 1
        if self.ejec_idx > self.orbiter_count then
            self.ejec_idx = 1
        end

        table.insert(self.recentimpacts, {absTime, meteor_data.endPos})
        meteor_scene.meteor_data = nil
    end
end

function meteorite:onray(ro, rd)
    local ret = nil
    if self.planet.onray then
        ret = self.planet:onray(ro, rd)
        if ret then
            if not stren then stren = .1 end
            local word = randomString(6)
            if true then
                local hp = self.planet.hitpt
                print(string.format("    {%q, %s, %g, %g},",
                    word,
                    string.format("{%g,%g,%g}", hp[1], hp[2], hp[3]),
                    .8,
                    stren))
            end
            self:launchMeteor(self.planet.hitpt, 1, stren, word)
            stren = stren + .2
            if stren > 1 then stren = .2 end
        end
    end
end

return meteorite
