--[[ comets_in_space2.lua

    
]]
comets_in_space2 = {}

comets_in_space2.__index = comets_in_space2

function comets_in_space2.new(...)
    local self = setmetatable({}, comets_in_space2)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

local CometLib = require("scene.comet")
local StarfieldLib = require("scene.starfield")
local mm = require("util.matrixmath")

function comets_in_space2:init(subdivs)
    self.comet = CometLib.new(subdivs)
    self.comx = 0
    self.stars = StarfieldLib.new()
    self.stars.xvel = .5
    self.reset_flag = 0
    self.comet_vel = -3
    self.wordlist_idx = 0

    self.cometData = {
        {
            {"X@#%$^!",{0,0,0}},
            {"X7647axx",{1,2,0}},
            {"Xabd6472",{2,4,0}},
            {"Xabd6473",{3,6,0}},
            {"Xabd6474",{4,8,0}},
            {"Xabd6475",{5,10,0}},
        },
        {
            {"first",{0,0,0}},
            {"second",{1,2,0}},
            {"third",{2,4,0}},
            {"fourth",{3,6,0}},
        },
        {
            {"AAA",{0,0,0}},
            {"BBB",{1,2,0}},
            {"CCC",{2,4,0}},
            {"DDD",{3,6,0}},
        },
    }
end

function comets_in_space2:setDataDirectory(dir)
    self.dataDir = dir
    self.comet:setDataDirectory(dir)

    -- Load strings from config file
    local strfilename = 'flying_comet_data.lua'
    local fullname = self.dataDir..'/'..strfilename
    local f = loadfile(fullname)
    if not f then
        print("File not found:", fullname)
        return
    end
    f()
    self.cometData = flying_comets.cometData
end

function comets_in_space2:initGL()
    self.comet:initGL()
    self.stars:initGL()
end

function comets_in_space2:exitGL()
    self.comet:exitGL()
    self.stars:exitGL()
end

function comets_in_space2:renderEye(model, view, proj)
    self:handleResetStateFlag()

    local wordlist = self.cometData[self.wordlist_idx]
    local i=0
    for k,v in pairs(wordlist) do
        local word = v[1]
        local pos = v[2]
        local m = {}
        for i=1,16 do m[i] = model[i] end
        mm.glh_translate(m, 10,-8,-10)
        mm.glh_translate(m, pos[1], pos[2], pos[3])
        local x = self.comx
        x = x * (1+.1*i)
        mm.glh_translate(m, x, 0, 0)
        local sz = .3
        --mm.glh_scale(m, sz,sz,sz)
        self.comet.core.word = word
        self.comet.halo.phase = i
        self.comet.halo.timescale = 1+.4*math.sin(i+1)
        i = i+1

        self.comet:renderEye(m, view, proj)
    end
    self.stars:renderEye(model, view, proj)
end

function comets_in_space2:timestep(absTime, dt)
    self.comx = self.comx + dt * self.comet_vel
    self.comet:timestep(absTime, dt)
    self.stars:timestep(absTime, dt)
end

function comets_in_space2:keypressed(key, scancode, action, mods)
    local factor = 1.2
    if key == string.byte(';') then
        self.comet_vel = self.comet_vel / factor
        print('comet_vel',self.comet_vel)
    elseif key == string.byte("'") then
        self.comet_vel = self.comet_vel * factor
        print('comet_vel',self.comet_vel)
    elseif key == 259 then -- bksp
        self.reset_flag = self.reset_flag + 1
    end
    self.stars:keypressed(key, scancode, action, mods)
end

function comets_in_space2:handleResetStateFlag()
    -- Check against last value
    -- For resetting state from sync tracker
    if self.last_reset_flag ~= self.reset_flag then
        self.comx = 0
        self.wordlist_idx = self.wordlist_idx + 1
        if self.wordlist_idx > #self.cometData then
            self.wordlist_idx = 1
        end
    end
    self.last_reset_flag = self.reset_flag
end

return comets_in_space2
