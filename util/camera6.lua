-- camera6.lua
-- Simple animated motion

local mm = require 'util.matrixmath'

camera6 = {}
camera6.__index = camera6

function camera6.new(...)
    local self = setmetatable({}, camera6)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function camera6:init()
    self.swing_angle = 0
end

function camera6:reset()
    self:init()
end

function camera6:timestep(absTime, dt)
    self.swing_angle = 180 * math.sin(.9*absTime)
end

function camera6:getViewMatrix()
    local v = {}
    mm.make_identity_matrix(v)

    local d = self.approach_dist
    mm.glh_rotate(v, self.swing_angle, 0,1,0)
    mm.glh_translate(v, 0,0,5)

    return v
end

return camera6
