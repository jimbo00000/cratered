-- camera5.lua
-- Simple animated motion

local mm = require 'util.matrixmath'

camera5 = {}
camera5.__index = camera5

function camera5.new(...)
    local self = setmetatable({}, camera5)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function camera5:init()
    self.approach_dist = 15
end

function camera5:reset()
    self:init()
end

function camera5:timestep(absTime, dt)
    self.approach_dist = 1+3*math.abs(math.sin(.1*absTime))
end

function camera5:getViewMatrix()
    local v = {}
    mm.make_identity_matrix(v)

    local d = self.approach_dist
    local pos = {-d,-d,-d}
    mm.glh_translate(v, pos[1], pos[2], pos[3])
    mm.glh_rotate(v, 180+45, 0,1,0)
    mm.glh_rotate(v, 45-10, 1,0,0) -- todo: what??
    -- todo: fix glh_lookat

    return v
end

return camera5
